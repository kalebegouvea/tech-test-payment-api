using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentApi.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public Vendedor? IdDoVendedor { get; set; }
        public DateTime? Data { get; set; }
        public string? IdentificadorPedido { get; set; }
        public string? Itens { get; set; }
        //Ao menos o item Status deve ser informado
        public EnumStatusVenda Status { get; set; }
    }
}